public class ByteConverters {
	public static byte ToUint8(int value) {
		byte val = (byte)value;
		return (byte) (val & 0xFF);
	}
	
	public static byte ToInt8(int value) {
		return (byte) value;
	}
	
	public static byte[] ToUint16(int value) {
		byte[] vals = new byte[2];
		vals[1]=(byte)((byte)value & 0xFF);
		vals[0]=(byte)(((byte)value>>8) & 0xFF);
		return vals;
	}
	
	public static byte[] ToInt16(int val){
		byte[] vals = new byte[2];
		vals[1]= (byte)val;
		vals[0]= (byte)((byte) val >> 8);
		return vals;
	}
	
	public static byte[] ToFloat(float val){
		byte[] vals = new byte[3];
		byte[] mantissa = ByteConverters.ToUint16(Math.round((val * 100)));
		byte exponent = ByteConverters.ToUint8(-2);
		vals[0] = mantissa[0];
		vals[1] = mantissa[1];
		vals[2] = exponent;
		return vals;
	}
}
