import java.util.Vector;

public class Mesh {
	
	//Holds element Data
	public Vector<Float> Verts = new Vector<Float>(); 
	public Vector<Float> TextCoords = new Vector<Float>();
	public Vector<Float> Normals = new Vector<Float>();
	//Texture data
	public String texture = "";
	
	Mesh() {}
	
	public String toString(){
		return	  "Vertices: "	+ this.Verts.toString() + '\n'
				+ "TextCoords: "	+ this.TextCoords.toString() + '\n'
				+ "Normals: "	+ this.Normals.toString() + '\n'
				+ "Texture: "	+ this.texture + '\n';
	}
}
