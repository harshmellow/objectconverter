import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

/**
 * @link = http://paulbourke.net/dataformats/obj/
 * 
 *       Basic Info
 *       <h2>Vertex Data</h2>
 *       <ol>
 *       <li>v = geometric vertices
 *       <li>vt = texture vertices
 *       <li>vn = vertex normals
 *       </ol>
 * 
 *       p = point l = line f = face
 * @author Harshmellow
 *
 */
public class ObjConverter {
	String basePath;
	String fileName;
	private Hashtable<String, String> matTextures = new Hashtable<String, String>();
	private Vector<Mesh> meshes = new Vector<Mesh>();

	ObjConverter() {
	}

	ObjConverter(String path) {
		// Separate path into base path and file name
		this.basePath = path.substring(0, path.lastIndexOf('\\'));
		this.fileName = path.substring(path.lastIndexOf('\\') + 1, path.lastIndexOf(('.')));
		// Read in file
		Vector<String[]> fileData = readFile(path);
		// Iterate over each line in the file
		Mesh currMesh = new Mesh();
		Vector<Float> verts = new Vector<Float>();
		Vector<Float> texCoords = new Vector<Float>();
		Vector<Float> normals = new Vector<Float>();
		for (int i = 0; i < fileData.size(); i++) {
			// mtllib: Get material resources
			if (fileData.get(i).length == 2 && fileData.get(i)[0].equals("mtllib")) {
				this.matTextures = readMTL(this.basePath + '\\' + fileData.get(i)[1]);
			}
			// g: Create new mesh
			if (fileData.get(i).length == 2 && (fileData.get(i)[0].equals("g") || fileData.get(i)[0].equals("o"))) {
				currMesh = new Mesh();
				meshes.add(currMesh);
			}
			// usemtl: Set the current mesh's texture
			if (fileData.get(i).length == 2 && fileData.get(i)[0].equals("usemtl")) {
				currMesh.texture = this.matTextures.get(fileData.get(i)[1]);
			}
			// v: Get Vertices and Store
			if (fileData.get(i).length == 4 && fileData.get(i)[0].equals("v")) {
				float vx = Float.parseFloat(fileData.get(i)[1]);
				float vy = Float.parseFloat(fileData.get(i)[2]);
				float vz = Float.parseFloat(fileData.get(i)[3]);
				verts.add(vx);
				verts.add(vy);
				verts.add(vz);
			}
			// vt: Get Texture-Vertices and Store
			if (fileData.get(i).length == 3 && fileData.get(i)[0].equals("vt")) {
				float vx = Float.parseFloat(fileData.get(i)[1]);
				float vy = Float.parseFloat(fileData.get(i)[2]);
				texCoords.add(vx);
				texCoords.add(vy);
			}
			// vn: Get Vertex-Normals and Stores
			if (fileData.get(i).length == 4 && fileData.get(i)[0].equals("vn")) {
				float vx = Float.parseFloat(fileData.get(i)[1]);
				float vy = Float.parseFloat(fileData.get(i)[2]);
				float vz = Float.parseFloat(fileData.get(i)[3]);
				normals.add(vx);
				normals.add(vy);
				normals.add(vz);
			}
			// f: Add coodinates to mesh
			if (fileData.get(i).length == 4 && fileData.get(i)[0].equals("f")) {
				String[] coord1 = fileData.get(i)[1].split("/");
				String[] coord2 = fileData.get(i)[2].split("/");
				String[] coord3 = fileData.get(i)[3].split("/");
				currMesh.Verts.add(verts.get((Integer.parseInt(coord1[0]) - 1) * 3));
				currMesh.Verts.add(verts.get((Integer.parseInt(coord1[0]) - 1) * 3 + 1));
				currMesh.Verts.add(verts.get((Integer.parseInt(coord1[0]) - 1) * 3 + 2));
				currMesh.Verts.add(verts.get((Integer.parseInt(coord2[0]) - 1) * 3));
				currMesh.Verts.add(verts.get((Integer.parseInt(coord2[0]) - 1) * 3 + 1));
				currMesh.Verts.add(verts.get((Integer.parseInt(coord2[0]) - 1) * 3 + 2));
				currMesh.Verts.add(verts.get((Integer.parseInt(coord3[0]) - 1) * 3));
				currMesh.Verts.add(verts.get((Integer.parseInt(coord3[0]) - 1) * 3 + 1));
				currMesh.Verts.add(verts.get((Integer.parseInt(coord3[0]) - 1) * 3 + 2));
				if (texCoords.size() != 0) {
					currMesh.TextCoords.add(texCoords.get((Integer.parseInt(coord1[1]) - 1) * 2));
					currMesh.TextCoords.add(texCoords.get((Integer.parseInt(coord1[1]) - 1) * 2 + 1));
					currMesh.TextCoords.add(texCoords.get((Integer.parseInt(coord2[1]) - 1) * 2));
					currMesh.TextCoords.add(texCoords.get((Integer.parseInt(coord2[1]) - 1) * 2 + 1));
					currMesh.TextCoords.add(texCoords.get((Integer.parseInt(coord3[1]) - 1) * 2));
					currMesh.TextCoords.add(texCoords.get((Integer.parseInt(coord3[1]) - 1) * 2 + 1));
				}
				if (normals.size() != 0) {
					currMesh.Normals.add(normals.get((Integer.parseInt(coord1[2]) - 1) * 3));
					currMesh.Normals.add(normals.get((Integer.parseInt(coord1[2]) - 1) * 3 + 1));
					currMesh.Normals.add(normals.get((Integer.parseInt(coord1[2]) - 1) * 3 + 2));
					currMesh.Normals.add(normals.get((Integer.parseInt(coord2[2]) - 1) * 3));
					currMesh.Normals.add(normals.get((Integer.parseInt(coord2[2]) - 1) * 3 + 1));
					currMesh.Normals.add(normals.get((Integer.parseInt(coord2[2]) - 1) * 3 + 2));
					currMesh.Normals.add(normals.get((Integer.parseInt(coord3[2]) - 1) * 3));
					currMesh.Normals.add(normals.get((Integer.parseInt(coord3[2]) - 1) * 3 + 1));
					currMesh.Normals.add(normals.get((Integer.parseInt(coord3[2]) - 1) * 3 + 2));
				}
			}
		}
	}

	/**
	 * Tested Y[X] N[]
	 * 
	 * TODO: Fix to take in console cmmds
	 * 
	 * @param fileName
	 *            -> fileName
	 * @return Vector<String[]> of data
	 */

	public Vector<String[]> readFile(String fileName) {
		Vector<String[]> output = new Vector<String[]>();
		try {
			// Open file
			BufferedReader buffer = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = buffer.readLine()) != null) {
				// Split line
				String[] splitLine = line.split(" ");
				// Remove any entries with undesirable text from each line
				Vector<String> tempLine = new Vector<String>();
				for (int i = 0; i < splitLine.length; i++) {
					if (splitLine[i].length() > 0 && splitLine[i] != "\t") {
						//Remove any undesirable characters from each entry
						String tempEntry = "";
						for (int j = 0; j < splitLine[i].length(); j++) {
							if (splitLine[i].toCharArray()[j] != '\t')
								tempEntry += splitLine[i].toCharArray()[j];
						}
						splitLine[i] = tempEntry;
						tempLine.add(splitLine[i]);
					}
				}
				splitLine = tempLine.toArray(new String[tempLine.size()]);
				// Add line to output
				output.add(splitLine);
			}
			// Close file
			buffer.close();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return output;
	}

	/**
	 * 
	 * 
	 * @param fileName
	 * @return String[] of all texture resources
	 */
	private Hashtable<String, String> readMTL(String path) {
		// Read file
		Vector<String[]> input = readFile(path);
		// Add texture paths to output using material names as keys
		Hashtable<String, String> output = new Hashtable<String, String>();
		String currMat = "";
		for (int i = 0; i < input.size(); i++) {
			// newmtl: Set current material
			if (input.get(i).length == 2 && input.get(i)[0].equals("newmtl")) {
				currMat = input.get(i)[1];
				output.put(currMat, "");
			}
			// map_Kd: Add texpath to hashtable, using current material as key
			if (input.get(i).length == 2 && input.get(i)[0].equals("map_Kd")) {
				output.put(currMat, input.get(i)[1]);
			}
		}
		return output;
	}

	public void writeFile() {
		try {
			DataOutputStream out = new DataOutputStream(
					new FileOutputStream(basePath.toString() + "\\" + this.fileName + ".dat"));

			out.write('S');
			out.write('M');
			out.write('S');
			out.write('H');
			out.write(ByteConverters.ToUint8(this.meshes.size()));
			for (int i = 0; i < this.meshes.size(); i++) {
				Mesh currMesh = meshes.get(i);
				out.write(ByteConverters.ToUint8(currMesh.texture.length()));
				out.write(currMesh.texture.getBytes());
				out.write(ByteConverters.ToUint16(currMesh.Verts.size() / 3));
				for (int j = 0; j < currMesh.Verts.size() / 3; j++) {
					out.write(ByteConverters.ToFloat(currMesh.Verts.get(j * 3)));
					out.write(ByteConverters.ToFloat(currMesh.Verts.get(j * 3 + 1)));
					out.write(ByteConverters.ToFloat(currMesh.Verts.get(j * 3 + 2)));
				}
				for (int j = 0; j < currMesh.TextCoords.size() / 2; j++) {
					out.write(ByteConverters.ToFloat(currMesh.TextCoords.get(j * 2)));
					out.write(ByteConverters.ToFloat(currMesh.TextCoords.get(j * 2 + 1)));
					out.write(ByteConverters.ToFloat(0));
				}
				for (int j = 0; j < currMesh.Normals.size() / 3; j++) {
					out.write(ByteConverters.ToFloat(currMesh.Normals.get(j * 3)));
					out.write(ByteConverters.ToFloat(currMesh.Normals.get(j * 3 + 1)));
					out.write(ByteConverters.ToFloat(currMesh.Normals.get(j * 3 + 2)));
				}
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public Vector<Mesh> getMeshes() {
		return this.meshes;
	}

	public Hashtable<String, String> getMatTextures() {
		return this.matTextures;
	}

	public String getName() {
		return this.fileName;
	}
}

class ObjTester {
	public static void main(String[] args) {
		System.out.println("Models to Process: " + args.length);
		System.out.println("------\n\n\n-------");
		float startTime = System.nanoTime();
		for (int i = 0; i < args.length; i++) {
			ObjConverter obj = new ObjConverter(args[i]);
			obj.writeFile();
			System.out.println("Model Name: " + obj.getName());
			System.out.println("Number of Meshes: " + obj.getMeshes().size());
			System.out.println("Textures: " + obj.getMatTextures().values());
			System.out.println("------\n\n\n-------");
		}
		System.out.println("Finished in " + Math.round((System.nanoTime() - startTime) / 1000000000 * 100) / 100 + " seconds.");
	}
}